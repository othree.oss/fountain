import {readJson, asBuffer, toFile, writeToStream} from '../src'
import * as Stream from 'stream'
import * as fs from 'fs'

describe('asBuffer', () => {
    it('should read a stream into a buffer', async () => {
        const expectedBuffer = Buffer.from('hello world', 'utf-8')
        const readable = new Stream.Readable()
        readable._read = () => {
        }
        readable.push(expectedBuffer)
        readable.push(null)

        const buffer = await asBuffer(readable)

        expect(buffer).toStrictEqual(expectedBuffer)
    })

    it('should reject the promise if it fails', async () => {
        const expectedBuffer = Buffer.from('hello world', 'utf-8')
        const readable = new Stream.Readable()
        readable._read = () => {
        }
        readable.push(expectedBuffer)
        readable.push(null)
        readable.destroy(new Error('KAPOW!'))

        await expect(() => asBuffer(readable)).rejects.toThrow(new Error('KAPOW!'))
    })
})

describe('readJson', () => {
    const expectedObject = {
        something: 'valuable'
    }
    it('should read a stream and deserialize it as json', async () => {
        const expectedBuffer = Buffer.from(JSON.stringify(expectedObject), 'utf-8')
        const readable = new Stream.Readable()
        readable._read = () => {
        }
        readable.push(expectedBuffer)
        readable.push(null)

        const maybeObject = await readJson(asBuffer)(readable)

        expect(maybeObject.get()).toStrictEqual(expectedObject)
        expect(maybeObject.isPresent).toBeTruthy()
    })
})

describe('writeToStream', () => {
    it('should pipe a readable stream into a writable stream', async () => {
        const readable = new Stream.PassThrough()
        const writable = new Stream.Writable()
        const mockedWrite = jest.fn().mockReturnValue(true)
        writable._write = mockedWrite

        setTimeout(() => {
            readable.emit('data', 'file contents')
            readable.emit('end')
        }, 100)

        const writeStream = await writeToStream(readable, writable)

        expect(writeStream).toBeTruthy()
        expect(mockedWrite).toHaveBeenCalledTimes(1)
        expect(mockedWrite.mock.calls[0][0]).toStrictEqual(Buffer.from('file contents'))
    })

    it('should reject if a stream error occurs', async () => {
        const readable = new Stream.PassThrough()
        const writable = new Stream.Writable()
        const mockedWrite = jest.fn().mockReturnValue(true)
        writable._write = mockedWrite

        setTimeout(() => {
            readable.emit('error', new Error('Cannot read stream!'))
        }, 100)

        const saveFilePromise = writeToStream(readable, writable)

        await expect(saveFilePromise).rejects.toEqual(new Error('Cannot read stream!'))
    })
})

describe('toFile', () => {
    jest.mock('fs')

    it('should write the stream to a file', async () => {
        const readable = new Stream.PassThrough()
        const writable = new Stream.PassThrough()
        const writeToStream = jest.fn().mockResolvedValue(true)
        const createWriteStream = jest.fn().mockReturnValueOnce(writable)

        // @ts-ignore
        fs.createWriteStream = createWriteStream

        const savedFile = await toFile(writeToStream)(readable, 'test.json')

        expect(savedFile).toEqual('test.json')
        expect(createWriteStream).toHaveBeenCalledTimes(1)
        expect(createWriteStream).toHaveBeenCalledWith('test.json')
    })
})