import {Readable, Writable} from 'stream'
import {Optional, TryAsync} from '@othree.io/optional'
import {createWriteStream} from 'fs'

export const asBuffer = (stream: Readable): Promise<Buffer> => {
    return new Promise<Buffer>((resolve, reject) => {
        const buffer = Array<Uint8Array>()
        stream.on('data', chunk => buffer.push(chunk))
        stream.on('end', () => resolve(Buffer.concat(buffer)))
        stream.on('error', error => {
            reject(error)
        })
    })
}

export const readJson = (asBuffer: (stream: Readable) => Promise<Buffer>) =>
    (stream: Readable): Promise<Optional<any>> => {
        return TryAsync(async () => {
            const buffer = await asBuffer(stream)

            return JSON.parse(String(buffer))
        })
    }

export const writeToStream = (stream: Readable, writeStream: Writable): Promise<boolean> => {
    return new Promise<boolean>((resolve, reject) => {
        stream.pipe(writeStream)
        stream.on('end', () => resolve(true))
        stream.on('error', error => {
            reject(error)
        })
    })
}

export const toFile = (writeToStream: (readableStream: Readable, writeStream: Writable) => Promise<boolean>) =>
    (readableStream: Readable, filePath: string): Promise<string> => {
        const writeStream = createWriteStream(filePath)
        return writeToStream(readableStream, writeStream)
            .then(_ => filePath)
    }