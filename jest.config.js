module.exports = {
    transform: {'^.+\\.ts?$': 'ts-jest'},
    testEnvironment: 'node',
    testRegex: '/test/.*\\.(test|spec)?\\.(ts|tsx)$',
    moduleFileExtensions: ['ts', 'tsx', 'js', 'jsx', 'json', 'node'],
    modulePathIgnorePatterns: [
        '<rootDir>/lib/'
    ],
    collectCoverage: true,
    coverageDirectory: './coverage',
    coverageReporters: ['html', 'text', 'text-summary'],
    coverageThreshold: {
        global: {
            branches: Number(process.env.CODE_COVERAGE_PERCENTAGE) || 100,
            functions: Number(process.env.CODE_COVERAGE_PERCENTAGE) || 100,
            lines: Number(process.env.CODE_COVERAGE_PERCENTAGE) || 100,
            statements: Number(process.env.CODE_COVERAGE_PERCENTAGE) || 100
        }
    }
}
